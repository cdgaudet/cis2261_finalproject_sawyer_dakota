﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyPlaceSMS_Sawyer
{
    class Course
    {
          
        private int courseId;
        private string courseName;
        private int teacherId;
        private int programId;
        private int credit;
        private int term;
        public Course()
        {
        }
        public Course(int courseId, string courseName, int  teacherId,int programId,int credit,int term)
        {
            CourseId = courseId;
            CourseName = courseName;
            TeacherId = teacherId;
            ProgramId = programId;
            Credit = credit;
            Term = term;
        }

        public int CourseId { get => courseId; set => courseId = value; }
        public string CourseName { get => courseName; set => courseName = value; }
        public int TeacherId { get => teacherId; set => teacherId = value; }
        public int ProgramId { get => programId; set => programId = value; }
        public int Credit { get => credit; set => credit = value; }
        public int Term { get => term; set => term = value; }

        public override string ToString()
        {
            return CourseName ;
        }
    }
}

