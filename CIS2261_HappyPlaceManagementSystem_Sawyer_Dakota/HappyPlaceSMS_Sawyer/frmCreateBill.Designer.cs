﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmCreateBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateBill));
            this.txtAssignedStudent = new System.Windows.Forms.TextBox();
            this.lblAssignedStudent = new System.Windows.Forms.Label();
            this.lblCreateBill = new System.Windows.Forms.Label();
            this.btnAddBill = new System.Windows.Forms.Button();
            this.txtBillAmount = new System.Windows.Forms.TextBox();
            this.lblBillAmount = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtAssignedStudent
            // 
            this.txtAssignedStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssignedStudent.Location = new System.Drawing.Point(157, 128);
            this.txtAssignedStudent.Margin = new System.Windows.Forms.Padding(2);
            this.txtAssignedStudent.Name = "txtAssignedStudent";
            this.txtAssignedStudent.Size = new System.Drawing.Size(345, 26);
            this.txtAssignedStudent.TabIndex = 44;
            // 
            // lblAssignedStudent
            // 
            this.lblAssignedStudent.AutoSize = true;
            this.lblAssignedStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssignedStudent.Location = new System.Drawing.Point(11, 128);
            this.lblAssignedStudent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAssignedStudent.Name = "lblAssignedStudent";
            this.lblAssignedStudent.Size = new System.Drawing.Size(140, 20);
            this.lblAssignedStudent.TabIndex = 43;
            this.lblAssignedStudent.Text = "Assigned Student:";
            // 
            // lblCreateBill
            // 
            this.lblCreateBill.AutoSize = true;
            this.lblCreateBill.Font = new System.Drawing.Font("Microsoft YaHei", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateBill.Location = new System.Drawing.Point(262, 25);
            this.lblCreateBill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCreateBill.Name = "lblCreateBill";
            this.lblCreateBill.Size = new System.Drawing.Size(129, 31);
            this.lblCreateBill.TabIndex = 40;
            this.lblCreateBill.Tag = "lblSelectStudent";
            this.lblCreateBill.Text = "Create Bill";
            // 
            // btnAddBill
            // 
            this.btnAddBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBill.Location = new System.Drawing.Point(200, 317);
            this.btnAddBill.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddBill.Name = "btnAddBill";
            this.btnAddBill.Size = new System.Drawing.Size(226, 28);
            this.btnAddBill.TabIndex = 36;
            this.btnAddBill.Tag = "btnAddBill";
            this.btnAddBill.Text = "Create Bill";
            this.btnAddBill.UseVisualStyleBackColor = true;
            this.btnAddBill.Click += new System.EventHandler(this.btnAddBill_Click);
            // 
            // txtBillAmount
            // 
            this.txtBillAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBillAmount.Location = new System.Drawing.Point(157, 90);
            this.txtBillAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtBillAmount.Name = "txtBillAmount";
            this.txtBillAmount.Size = new System.Drawing.Size(345, 26);
            this.txtBillAmount.TabIndex = 33;
            // 
            // lblBillAmount
            // 
            this.lblBillAmount.AutoSize = true;
            this.lblBillAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillAmount.Location = new System.Drawing.Point(11, 90);
            this.lblBillAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBillAmount.Name = "lblBillAmount";
            this.lblBillAmount.Size = new System.Drawing.Size(93, 20);
            this.lblBillAmount.TabIndex = 32;
            this.lblBillAmount.Text = "Bill Amount:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(11, 167);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(93, 20);
            this.lblDescription.TabIndex = 45;
            this.lblDescription.Text = "Description:";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(157, 169);
            this.rtbDescription.Margin = new System.Windows.Forms.Padding(2);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(345, 128);
            this.rtbDescription.TabIndex = 46;
            this.rtbDescription.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(126, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // frmCreateBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.rtbDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.txtAssignedStudent);
            this.Controls.Add(this.lblAssignedStudent);
            this.Controls.Add(this.lblCreateBill);
            this.Controls.Add(this.btnAddBill);
            this.Controls.Add(this.txtBillAmount);
            this.Controls.Add(this.lblBillAmount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCreateBill";
            this.Text = "Happy Place College - Create Bill";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAssignedStudent;
        private System.Windows.Forms.Label lblAssignedStudent;
        private System.Windows.Forms.Label lblCreateBill;
        private System.Windows.Forms.Button btnAddBill;
        private System.Windows.Forms.TextBox txtBillAmount;
        private System.Windows.Forms.Label lblBillAmount;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}