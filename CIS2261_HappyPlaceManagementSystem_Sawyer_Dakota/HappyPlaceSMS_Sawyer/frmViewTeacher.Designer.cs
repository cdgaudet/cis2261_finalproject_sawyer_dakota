﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmViewTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewTeacher));
            this.dataGridViewStudentList = new System.Windows.Forms.DataGridView();
            this.teacherID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherProgram = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewStudentList
            // 
            this.dataGridViewStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.teacherID,
            this.studentLastName,
            this.studentFirstName,
            this.teacherProgram});
            this.dataGridViewStudentList.Location = new System.Drawing.Point(2, 1);
            this.dataGridViewStudentList.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewStudentList.Name = "dataGridViewStudentList";
            this.dataGridViewStudentList.RowHeadersWidth = 51;
            this.dataGridViewStudentList.RowTemplate.Height = 24;
            this.dataGridViewStudentList.Size = new System.Drawing.Size(793, 365);
            this.dataGridViewStudentList.TabIndex = 1;
            // 
            // teacherID
            // 
            this.teacherID.HeaderText = "Teacher ID";
            this.teacherID.MinimumWidth = 6;
            this.teacherID.Name = "teacherID";
            this.teacherID.Width = 120;
            // 
            // studentLastName
            // 
            this.studentLastName.HeaderText = "Last Name";
            this.studentLastName.MinimumWidth = 6;
            this.studentLastName.Name = "studentLastName";
            this.studentLastName.ReadOnly = true;
            this.studentLastName.Width = 150;
            // 
            // studentFirstName
            // 
            this.studentFirstName.HeaderText = "First Name";
            this.studentFirstName.MinimumWidth = 6;
            this.studentFirstName.Name = "studentFirstName";
            this.studentFirstName.ReadOnly = true;
            this.studentFirstName.Width = 150;
            // 
            // teacherProgram
            // 
            this.teacherProgram.HeaderText = "Assigned Course";
            this.teacherProgram.MinimumWidth = 6;
            this.teacherProgram.Name = "teacherProgram";
            this.teacherProgram.ReadOnly = true;
            this.teacherProgram.Width = 320;
            // 
            // frmViewTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 366);
            this.Controls.Add(this.dataGridViewStudentList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmViewTeacher";
            this.Text = "Happy Place College - View Teacher";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewStudentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherID;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherProgram;
    }
}