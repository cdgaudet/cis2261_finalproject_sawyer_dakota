﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmCourseList : Form
    {
        public frmCourseList()
        {
            InitializeComponent();
            PopulateCourseCatalogue();
        }

        public void PopulateCourseCatalogue()
        {

            //build connection to CISDB
            //create connection string for Cottages database
            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            //  ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT COURSE.COURSE_ID, COURSE.COURSE_NAME, TEACHER.TEACHER_LNAME, TEACHER.TEACHER_FNAME, DEPARTMENT.DEPARTMENT_NAME FROM TEACHER INNER JOIN((DEPARTMENT INNER JOIN PROGRAM ON DEPARTMENT.DEPARTMENT_ID = PROGRAM.DEPARTMENT_ID) INNER JOIN COURSE ON PROGRAM.PROGRAM_ID = COURSE.PROGRAM_ID) ON TEACHER.TEACHER_ID = COURSE.TEACHER_ID ORDER BY DEPARTMENT.DEPARTMENT_NAME ASC ; "; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a guest object populate the firstName and personId attibutes
                    //  Guest gue = new Guest(dbReader["FirstName"].ToString(), dbReader["LastName"].ToString(), dbReader["Street"].ToString(), dbReader["City"].ToString(), dbReader["State"].ToString(), dbReader["Zip"].ToString(), dbReader["Phone"].ToString(), dbReader["email"].ToString(), dbReader["LastVisitDate"].ToString(), dbReader["Room"].ToString(), (int)dbReader["GuestID"]);

                    //load the Guest object per into the combobox
                    //when displayed the combo box will call toString by default on the Guest object.
                    //the toString  displays the FirstName and LastName of the person.
                    //   cmbSelectGuest.Items.Add(gue);
                  
                    dataGridViewCourseList.Rows.Add(dbReader["COURSE_ID"].ToString(), dbReader["COURSE_NAME"].ToString(), dbReader["TEACHER_LNAME"].ToString(), dbReader["TEACHER_FNAME"].ToString(), dbReader["DEPARTMENT_NAME"].ToString());
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
