﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyPlaceSMS_Sawyer
{
    class Teacher

    {
        private int teacherId;
        private string teacherfName;
        private string teacherlName;
        public Teacher(){
    }
        public Teacher(int teacherId,string teacherfName,string teacherLName)
        {
            TeacherId = teacherId;
            TeacherfName = teacherfName;
            TeacherlName = teacherLName;
        }

        public int TeacherId { get => teacherId; set => teacherId = value; }
        public string TeacherfName { get => teacherfName; set => teacherfName = value; }
        public string TeacherlName { get => teacherlName; set => teacherlName = value; }
        public override string ToString()
        {
            return TeacherfName + " " + TeacherlName;
        }
    }
}
