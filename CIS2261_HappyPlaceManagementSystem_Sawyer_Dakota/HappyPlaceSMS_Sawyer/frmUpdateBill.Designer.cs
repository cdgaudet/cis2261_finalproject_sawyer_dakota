﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmUpdateBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateBill));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtAssignedStudent = new System.Windows.Forms.TextBox();
            this.lblAssignedStudent = new System.Windows.Forms.Label();
            this.lblUpdateBill = new System.Windows.Forms.Label();
            this.btnUpdateBill = new System.Windows.Forms.Button();
            this.txtBillAmount = new System.Windows.Forms.TextBox();
            this.lblBillAmount = new System.Windows.Forms.Label();
            this.cmbBoxSelectBill = new System.Windows.Forms.ComboBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtAmountPaid = new System.Windows.Forms.TextBox();
            this.lblAmountPaid = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDateOfPayment = new System.Windows.Forms.DateTimePicker();
            this.lblBill = new System.Windows.Forms.Label();
            this.txtBillId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-4, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(225, 341);
            this.rtbDescription.Margin = new System.Windows.Forms.Padding(2);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(326, 50);
            this.rtbDescription.TabIndex = 5;
            this.rtbDescription.Text = "";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(76, 339);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(93, 20);
            this.lblDescription.TabIndex = 54;
            this.lblDescription.Text = "Description:";
            // 
            // txtAssignedStudent
            // 
            this.txtAssignedStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssignedStudent.Location = new System.Drawing.Point(225, 220);
            this.txtAssignedStudent.Margin = new System.Windows.Forms.Padding(2);
            this.txtAssignedStudent.Name = "txtAssignedStudent";
            this.txtAssignedStudent.Size = new System.Drawing.Size(326, 26);
            this.txtAssignedStudent.TabIndex = 2;
            // 
            // lblAssignedStudent
            // 
            this.lblAssignedStudent.AutoSize = true;
            this.lblAssignedStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssignedStudent.Location = new System.Drawing.Point(73, 220);
            this.lblAssignedStudent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAssignedStudent.Name = "lblAssignedStudent";
            this.lblAssignedStudent.Size = new System.Drawing.Size(140, 20);
            this.lblAssignedStudent.TabIndex = 52;
            this.lblAssignedStudent.Text = "Assigned Student:";
            // 
            // lblUpdateBill
            // 
            this.lblUpdateBill.AutoSize = true;
            this.lblUpdateBill.Font = new System.Drawing.Font("Microsoft YaHei", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateBill.Location = new System.Drawing.Point(235, 33);
            this.lblUpdateBill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUpdateBill.Name = "lblUpdateBill";
            this.lblUpdateBill.Size = new System.Drawing.Size(138, 31);
            this.lblUpdateBill.TabIndex = 51;
            this.lblUpdateBill.Tag = "lblSelectStudent";
            this.lblUpdateBill.Text = "Update Bill";
            // 
            // btnUpdateBill
            // 
            this.btnUpdateBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateBill.Location = new System.Drawing.Point(194, 405);
            this.btnUpdateBill.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateBill.Name = "btnUpdateBill";
            this.btnUpdateBill.Size = new System.Drawing.Size(226, 28);
            this.btnUpdateBill.TabIndex = 6;
            this.btnUpdateBill.Tag = "btnAddBill";
            this.btnUpdateBill.Text = "Update Bill";
            this.btnUpdateBill.UseVisualStyleBackColor = true;
            this.btnUpdateBill.Click += new System.EventHandler(this.btnUpdateBill_Click);
            // 
            // txtBillAmount
            // 
            this.txtBillAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBillAmount.Location = new System.Drawing.Point(225, 183);
            this.txtBillAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtBillAmount.Name = "txtBillAmount";
            this.txtBillAmount.Size = new System.Drawing.Size(326, 26);
            this.txtBillAmount.TabIndex = 1;
            // 
            // lblBillAmount
            // 
            this.lblBillAmount.AutoSize = true;
            this.lblBillAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillAmount.Location = new System.Drawing.Point(73, 183);
            this.lblBillAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBillAmount.Name = "lblBillAmount";
            this.lblBillAmount.Size = new System.Drawing.Size(93, 20);
            this.lblBillAmount.TabIndex = 48;
            this.lblBillAmount.Text = "Bill Amount:";
            // 
            // cmbBoxSelectBill
            // 
            this.cmbBoxSelectBill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSelectBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxSelectBill.FormattingEnabled = true;
            this.cmbBoxSelectBill.Location = new System.Drawing.Point(225, 105);
            this.cmbBoxSelectBill.Margin = new System.Windows.Forms.Padding(2);
            this.cmbBoxSelectBill.Name = "cmbBoxSelectBill";
            this.cmbBoxSelectBill.Size = new System.Drawing.Size(326, 28);
            this.cmbBoxSelectBill.TabIndex = 0;
            this.cmbBoxSelectBill.Tag = "cmbBoxSelectTeacher";
            this.cmbBoxSelectBill.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSelectBill_SelectedIndexChanged);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(76, 146);
            this.lblID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(30, 20);
            this.lblID.TabIndex = 58;
            this.lblID.Tag = "lblID";
            this.lblID.Text = "ID:";
            // 
            // txtAmountPaid
            // 
            this.txtAmountPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountPaid.Location = new System.Drawing.Point(225, 263);
            this.txtAmountPaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtAmountPaid.Name = "txtAmountPaid";
            this.txtAmountPaid.Size = new System.Drawing.Size(326, 26);
            this.txtAmountPaid.TabIndex = 3;
            // 
            // lblAmountPaid
            // 
            this.lblAmountPaid.AutoSize = true;
            this.lblAmountPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmountPaid.Location = new System.Drawing.Point(76, 263);
            this.lblAmountPaid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAmountPaid.Name = "lblAmountPaid";
            this.lblAmountPaid.Size = new System.Drawing.Size(104, 20);
            this.lblAmountPaid.TabIndex = 59;
            this.lblAmountPaid.Text = "Amount Paid:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(76, 301);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 20);
            this.label1.TabIndex = 61;
            this.label1.Text = "Date Of Payment:";
            // 
            // dtpDateOfPayment
            // 
            this.dtpDateOfPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateOfPayment.Location = new System.Drawing.Point(225, 301);
            this.dtpDateOfPayment.Margin = new System.Windows.Forms.Padding(2);
            this.dtpDateOfPayment.Name = "dtpDateOfPayment";
            this.dtpDateOfPayment.Size = new System.Drawing.Size(326, 26);
            this.dtpDateOfPayment.TabIndex = 4;
            // 
            // lblBill
            // 
            this.lblBill.AutoSize = true;
            this.lblBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBill.Location = new System.Drawing.Point(76, 105);
            this.lblBill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBill.Name = "lblBill";
            this.lblBill.Size = new System.Drawing.Size(33, 20);
            this.lblBill.TabIndex = 63;
            this.lblBill.Tag = "lblID";
            this.lblBill.Text = "Bill:";
            // 
            // txtBillId
            // 
            this.txtBillId.Enabled = false;
            this.txtBillId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBillId.Location = new System.Drawing.Point(225, 146);
            this.txtBillId.Margin = new System.Windows.Forms.Padding(2);
            this.txtBillId.Name = "txtBillId";
            this.txtBillId.Size = new System.Drawing.Size(326, 26);
            this.txtBillId.TabIndex = 64;
            // 
            // frmUpdateBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(610, 456);
            this.Controls.Add(this.txtBillId);
            this.Controls.Add(this.lblBill);
            this.Controls.Add(this.dtpDateOfPayment);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAmountPaid);
            this.Controls.Add(this.lblAmountPaid);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.cmbBoxSelectBill);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.rtbDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.txtAssignedStudent);
            this.Controls.Add(this.lblAssignedStudent);
            this.Controls.Add(this.lblUpdateBill);
            this.Controls.Add(this.btnUpdateBill);
            this.Controls.Add(this.txtBillAmount);
            this.Controls.Add(this.lblBillAmount);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmUpdateBill";
            this.Text = "Happy Place College - Update Bill";
            this.Load += new System.EventHandler(this.frmUpdateBill_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtAssignedStudent;
        private System.Windows.Forms.Label lblAssignedStudent;
        private System.Windows.Forms.Label lblUpdateBill;
        private System.Windows.Forms.Button btnUpdateBill;
        private System.Windows.Forms.TextBox txtBillAmount;
        private System.Windows.Forms.Label lblBillAmount;
        private System.Windows.Forms.ComboBox cmbBoxSelectBill;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtAmountPaid;
        private System.Windows.Forms.Label lblAmountPaid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDateOfPayment;
        private System.Windows.Forms.Label lblBill;
        private System.Windows.Forms.TextBox txtBillId;
    }
}