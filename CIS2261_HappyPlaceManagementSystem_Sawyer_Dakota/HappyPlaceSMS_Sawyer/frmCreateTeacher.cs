﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmCreateTeacher : Form
    {
        public frmCreateTeacher()
        {
            InitializeComponent();
        }

        //build connection to CISDB
        //create connection string for Cottages database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
        //Create OldDbConnection
        OleDbConnection dbConn;

        public void populateTeacherCombo()
        {
            ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT TEACHER_ID,TEACHER_FNAME,TEACHER_LNAME from TEACHER;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a teacher object populate the firstName and personId attibutes
                    Teacher tea = new Teacher((int)dbReader["TEACHER_ID"], dbReader["TEACHER_FNAME"].ToString(), dbReader["TEACHER_LNAME"].ToString());

                    //load the teacher object per into the combobox
                    //when displayed the combo box will call toString by default on the teacher object.
                    //the toString  displays the FirstName and LastName of the person.
                    cmbBoxSelectTeacher.Items.Add(tea);

                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ClearForm()
        {
            cmbBoxSelectTeacher.Items.Clear();
            txtTeacherFirstName.Clear();
            txtTeacherLastName.Clear();
            txtTeacherID.Clear();
            txtAssignedCourses.Clear();
        }

        private void addTeacher()
        {


            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from teachers table
                string sql;
                sql = "Insert into TEACHER(TEACHER_FNAME,TEACHER_LNAME,ASSIGNED_COURSES) Values (@TeacherFirstName,@TeacherLastName,@AssignedCourses);"; //note the two semicolons

                //create database command
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;

                //bind parameters
                dbCmd.Parameters.AddWithValue("@TeacherFirstName", txtTeacherFirstName.Text);
                dbCmd.Parameters.AddWithValue("@TeacherLastName", txtTeacherLastName.Text);
                dbCmd.Parameters.AddWithValue("@AssignedCourses", txtAssignedCourses.Text);



                //MessageBox.Show(txtPhone.Text);
                //execute insert. Check to see how many rows were affected
                int rowCount = dbCmd.ExecuteNonQuery();

                //close database connection
                dbConn.Close();
                if (rowCount == 1)
                {
                    MessageBox.Show("Record inserted successfully");
                    //this is where I want to update frmMain
                    populateTeacherCombo();

                }
                else
                {
                    MessageBox.Show("Error inserting record. Please try again.");
                    ClearForm();
                    cmbBoxSelectTeacher.ResetText();
                    populateTeacherCombo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private bool validateForm()
        {
            string errMsg = "";
            if (txtTeacherFirstName.Text == "")
            {
                errMsg += "Missing first name. \n";
            }
            if (txtTeacherLastName.Text == "")
            {
                errMsg += "Missing last name. \n";
            }
            if (txtAssignedCourses.Text == "")
            {
                errMsg += "Missing courses. \n";
            }



            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void PopulateTeacherRead()
        {
            //the combobox is populated with teacher objects. Cast the selected value as a teacher and then you can 
            //access the public property teacherId;
            int teacherSelection = ((Teacher)cmbBoxSelectTeacher.SelectedItem).TeacherId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from teachers table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the teacherId to search for
                sql = "SELECT(Select count(TEACHER_ID) from TEACHER where TEACHER_ID= " + teacherSelection + ") " +
                               "as rowCount, * from TEACHER where TEACHER_ID = " + teacherSelection + ";"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                // MessageBox.Show(sql);
                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes

                    txtTeacherID.Text = dbReader["TEACHER_ID"].ToString();
                    txtTeacherFirstName.Text = dbReader["TEACHER_FNAME"].ToString();
                    txtTeacherLastName.Text = dbReader["TEACHER_LNAME"].ToString();
                    txtAssignedCourses.Text = dbReader["ASSIGNED_COURSES"].ToString();
                    
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void frmCreateTeacher_Load(object sender, EventArgs e)
        {
            populateTeacherCombo();
            txtTeacherID.ReadOnly = true;
        }

        private void cmbBoxSelectTeacher_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateTeacherRead();
        }

        private void btnAddTeacher_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectTeacher.SelectedIndex >= 0)
            {
                ClearForm();
                cmbBoxSelectTeacher.ResetText();
                populateTeacherCombo();
            }
            else
            {
                if (validateForm() == true)
                {
                    addTeacher();
                }

            }
        }

        private void btnUpdateTeacher_Click(object sender, EventArgs e)
        {

            if (cmbBoxSelectTeacher.SelectedIndex > -1)
            {
                //Check to see if they want to update the current record
                DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    int currentTeacherID = Convert.ToInt32(txtTeacherID.Text);
                    //update the record
                    try
                    {
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to update selected teacher record
                        string sql;
                        sql = "UPDATE TEACHER SET TEACHER_FNAME = @FirstName, TEACHER_LNAME = @LastName, ASSIGNED_COURSES = @AssignedCourses WHERE TEACHER_ID = " + currentTeacherID + ";";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@FirstName", txtTeacherFirstName.Text);
                        dbCmd.Parameters.AddWithValue("@LastName", txtTeacherLastName.Text);
                        dbCmd.Parameters.AddWithValue("@AssignedCourses", txtAssignedCourses.Text);
                        dbCmd.Parameters.AddWithValue("@TeacherID", txtTeacherID.Text);

                        //execute update. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();
                        // MessageBox.Show(rowCount.ToString());
                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record updated successfully");
                            ClearForm();
                            cmbBoxSelectTeacher.ResetText();
                            populateTeacherCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error updating record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {

                    MessageBox.Show("Please select the teacher you want to update");
                }
            }
        }

        private void btnDeleteTeacher_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectTeacher.SelectedIndex > 0)
            {
                //Check to see if they want to delete the current record
                DialogResult result = MessageBox.Show("Do you want to delete this record?", "Delete Record",
                MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //update the record
                    try
                    {
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to delete selected teacher record
                        string sql;
                        sql = "Delete from TEACHER where TEACHER_ID = @TeacherID";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@TeacherID", txtTeacherID.Text);

                        //execute delete. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();

                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record deleted successfully");
                            ClearForm();
                            cmbBoxSelectTeacher.ResetText();
                            populateTeacherCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error deleting record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {

                MessageBox.Show("You need to select a teacher to delete");
            }
        }
    }
}
