﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmMain : Form
    {
        //  frmStudentList fs;

        //Logged in variable 
        public static bool loggedIn = false;

        public frmMain()
        {
            InitializeComponent();
        }
        private void frmMain_Load(object sender, EventArgs e)
        {
            logoutToolStripMenuItem.Visible = false;
        }

        private void createStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmCreateStudent fcs = new frmCreateStudent();
                fcs.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void viewStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmViewStudent fsl = new frmViewStudent();
                fsl.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void createTeacherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmCreateTeacher fct = new frmCreateTeacher();
                fct.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void createCourseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmCreateCourse fcc = new frmCreateCourse();
                fcc.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void viewCourseCatalogueToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmCourseList fcl = new frmCourseList();
                fcl.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
            
        }

        private void facultyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmFacultyList ffl = new frmFacultyList();
                ffl.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logoutToolStripMenuItem.Visible = false;
            loginToolStripMenuItem.Visible = true;
            MessageBox.Show("Have A Good Day");
        }
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLogin fln = new frmLogin();
            fln.Owner = this;
            fln.Show();
        }
        private void viewTeacherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmViewTeacher fvt = new frmViewTeacher();
                fvt.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void viewBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmViewBill fvb = new frmViewBill();
                fvb.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void listStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmStuList fsl = new frmStuList();
                fsl.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void createBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmCreateBill fcb = new frmCreateBill();
                fcb.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }

        private void updateBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                frmUpdateBill fub = new frmUpdateBill();
                fub.Show();
            }
            else
            {
                MessageBox.Show("Please login to access this data");
            }
        }
    }
}
