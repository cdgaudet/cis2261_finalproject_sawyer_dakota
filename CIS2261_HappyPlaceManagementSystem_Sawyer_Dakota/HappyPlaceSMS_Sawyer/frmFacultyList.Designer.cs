﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmFacultyList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFacultyList));
            this.dataGridViewFacultyList = new System.Windows.Forms.DataGridView();
            this.faculty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.department = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.courseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.term = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacultyList)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewFacultyList
            // 
            this.dataGridViewFacultyList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFacultyList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.faculty,
            this.department,
            this.courseName,
            this.term});
            this.dataGridViewFacultyList.Location = new System.Drawing.Point(2, 1);
            this.dataGridViewFacultyList.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewFacultyList.Name = "dataGridViewFacultyList";
            this.dataGridViewFacultyList.RowHeadersWidth = 51;
            this.dataGridViewFacultyList.RowTemplate.Height = 24;
            this.dataGridViewFacultyList.Size = new System.Drawing.Size(783, 365);
            this.dataGridViewFacultyList.TabIndex = 1;
            // 
            // faculty
            // 
            this.faculty.HeaderText = "Faculty";
            this.faculty.MinimumWidth = 6;
            this.faculty.Name = "faculty";
            this.faculty.ReadOnly = true;
            this.faculty.Width = 270;
            // 
            // department
            // 
            this.department.HeaderText = "Department";
            this.department.MinimumWidth = 6;
            this.department.Name = "department";
            this.department.ReadOnly = true;
            this.department.Width = 270;
            // 
            // courseName
            // 
            this.courseName.HeaderText = "Course Name";
            this.courseName.MinimumWidth = 6;
            this.courseName.Name = "courseName";
            this.courseName.ReadOnly = true;
            // 
            // term
            // 
            this.term.HeaderText = "Term";
            this.term.MinimumWidth = 6;
            this.term.Name = "term";
            // 
            // frmFacultyList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 366);
            this.Controls.Add(this.dataGridViewFacultyList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmFacultyList";
            this.Text = "Happy Place College - Faculty Report";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFacultyList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewFacultyList;
        private System.Windows.Forms.DataGridViewTextBoxColumn faculty;
        private System.Windows.Forms.DataGridViewTextBoxColumn department;
        private System.Windows.Forms.DataGridViewTextBoxColumn courseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn term;
    }
}