﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmCreateBill : Form
    {
        public frmCreateBill()
        {
            InitializeComponent();
        }

        private bool validateForm()
        {
            string errMsg = "";
            if (txtAssignedStudent.Text == "")
            {
                errMsg += "Missing Assigned Student. \n";
            }
            if (txtBillAmount.Text == "")
            {
                errMsg += "Missing Bill Amount. \n";
            }

            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void addCourse()
        {

            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Guests table
                string sql;
                sql = "INSERT INTO BILL(BILL_AMOUNT, ASSIGNED_STUDENT, DESCRIPTION) VALUES (@BillAmount, @AssignedStudent, @Description);"; //note the two semicolons

                //create database command
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;

                //bind parameters
                dbCmd.Parameters.AddWithValue("@BillAmount", txtBillAmount.Text);
                dbCmd.Parameters.AddWithValue("@AssignedStudent", txtAssignedStudent.Text);
                dbCmd.Parameters.AddWithValue("@Description", rtbDescription.Text);
                //execute insert. Check to see how many rows were affected
                int rowCount = dbCmd.ExecuteNonQuery();

                //close database connection
                dbConn.Close();
                if (rowCount == 1)
                {
                    MessageBox.Show("Record inserted successfully");
                    ClearForm();
                    //this is where I want to update frmMain
                }
                else
                {
                    MessageBox.Show("Error inserting record. Please try again.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void ClearForm()
        {
            txtAssignedStudent.Text = "";
            txtBillAmount.Text = "";
            rtbDescription.Text = "";
        }
        private void btnAddBill_Click(object sender, EventArgs e)
        {
            validateForm();
            addCourse();
            ClearForm();
        }
    }
}


