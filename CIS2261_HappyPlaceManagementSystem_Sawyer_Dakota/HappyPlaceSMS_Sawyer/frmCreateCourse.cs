﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmCreateCourse : Form
    {
        public frmCreateCourse()
        {
            InitializeComponent();
        }
        //build connection to CISDB
        //create connection string for Cottages database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
        //Create OldDbConnection
        OleDbConnection dbConn;
        private void cmbBoxSelectCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateCourseRead();
        }
        private void PopulateCourseRead()
        {
            //the combobox is populated with Guest objects. Cast the selected value as a Guest and then you can 
            //access the public property GuestId;
            int courseSelection = ((Course)cmbBoxSelectCourse.SelectedItem).CourseId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Guests table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the GuestId to search for
                sql = "SELECT(Select count(COURSE_ID) from COURSE where COURSE_ID= " + courseSelection + ") " +
                               "as rowCount, * from COURSE where COURSE_ID = " + courseSelection + ";"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                // MessageBox.Show(sql);
                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes

                    txtCourseID.Text = dbReader["COURSE_ID"].ToString();
                    txtCourseName.Text = dbReader["COURSE_NAME"].ToString();
                    txtTeacherID.Text = dbReader["TEACHER_ID"].ToString();
                    txtProgramID.Text = dbReader["PROGRAM_ID"].ToString();
                    txtCredit.Text = dbReader["CREDIT"].ToString();
                    txtTerm.Text = dbReader["TERM"].ToString();
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void btnAddCourse_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectCourse.SelectedIndex >= 0)
            {
                ClearForm();
                cmbBoxSelectCourse.ResetText();
                populateCourseCombo();
            }
            else
            {
                if (validateForm() == true)
                {
                    addCourse();
                }

            }
        }
        public void populateCourseCombo()
        {
            ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT COURSE_ID,COURSE_NAME,TEACHER_ID,PROGRAM_ID,CREDIT,TERM from  COURSE;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a guest object populate the firstName and personId attibutes
                        Course cou = new Course((int)dbReader["COURSE_ID"], dbReader["COURSE_NAME"].ToString(), (int)dbReader["TEACHER_ID"], (int)dbReader["PROGRAM_ID"], (int)dbReader["CREDIT"], (int)dbReader["TERM"]);

                    //load the Guest object per into the combobox
                    //when displayed the combo box will call toString by default on the Guest object.
                    //the toString  displays the FirstName and LastName of the person.
                    cmbBoxSelectCourse.Items.Add(cou);

                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void ClearForm()
        {
            cmbBoxSelectCourse.Items.Clear();
            txtCourseID.Clear();
            txtProgramID.Clear();
            txtTeacherID.Clear();
            txtCourseName.Clear();
            txtCredit.Clear();
            txtTerm.Clear();
        }

        private void addCourse()
        {


            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Guests table
                string sql;
                sql = "Insert into COURSE(COURSE_NAME,TEACHER_ID,PROGRAM_ID,CREDIT,TERM) Values (@CourseName,@TeacherId,@Program_Id,@Credit,@Term);"; //note the two semicolons

                //create database command
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;

                //bind parameters
                dbCmd.Parameters.AddWithValue("@CourseName", txtCourseName.Text);
                dbCmd.Parameters.AddWithValue("@TeacherId", txtTeacherID.Text);
                dbCmd.Parameters.AddWithValue("@Program_Id", txtProgramID.Text);
                dbCmd.Parameters.AddWithValue("@Credit", txtCredit.Text);
                dbCmd.Parameters.AddWithValue("@Term", txtTerm.Text);
                //MessageBox.Show(txtPhone.Text);
                //execute insert. Check to see how many rows were affected
                int rowCount = dbCmd.ExecuteNonQuery();

                //close database connection
                dbConn.Close();
                if (rowCount == 1)
                {
                    MessageBox.Show("Record inserted successfully");
                    //this is where I want to update frmMain
                    populateCourseCombo();

                }
                else
                {
                    MessageBox.Show("Error inserting record. Please try again.");
                    ClearForm();
                    cmbBoxSelectCourse.ResetText();
                    populateCourseCombo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private bool validateForm()
        {
            string errMsg = "";
            if (txtCourseName.Text == "")
            {
                errMsg += "Missing Course name. \n";
            }
            if (txtTeacherID.Text == "")
            {
                errMsg += "Missing teacher. \n";
            }
            if (txtProgramID.Text == "")
            {
                errMsg += "Missing program. \n";
            }
            if (txtCredit.Text == "")
            {
                errMsg += "Missing credit. \n";
            }
            if (txtTerm.Text == "")
            {
                errMsg += "Missing term. \n";
            }


            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void btnUpdateCourse_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectCourse.SelectedIndex > -1)
            {
                //Check to see if they want to update the current record
                DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //update the record
                    try
                    {
                        MessageBox.Show("Update");
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to update selected guest record
                        string sql;
                        sql = "Update COURSE set COURSE_NAME=@CourseName,TEACHER_ID=@TeacherId,PROGRAM_ID=@ProgramId,CREDIT=@Credit,TERM=@Term where COURSE_ID=@Course_id";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@CourseName", txtCourseName.Text);
                        dbCmd.Parameters.AddWithValue("@TeacherId", txtTeacherID.Text);
                        dbCmd.Parameters.AddWithValue("@ProgramId", txtProgramID.Text);
                        dbCmd.Parameters.AddWithValue("@Credit", txtCredit.Text);
                        dbCmd.Parameters.AddWithValue("@Term", txtTerm.Text);
                        dbCmd.Parameters.AddWithValue("@Course_id", txtCourseID.Text);
                        //  MessageBox.Show(dateTimeLastVisitDate.Value.ToShortDateString());
                        //MessageBox.Show(sql);
                        //execute update. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();
                        // MessageBox.Show(rowCount.ToString());
                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record updated successfully");
                            ClearForm();
                            cmbBoxSelectCourse.ResetText();
                            populateCourseCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error updating record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {

                    MessageBox.Show("Please select the guest you want to update");
                }
            }
        }

        private void btnDeleteCourse_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectCourse.SelectedIndex > 0)
            {
                //Check to see if they want to delete the current record
                DialogResult result = MessageBox.Show("Do you want to delete this record?", "Delete Record",
                MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //update the record
                    try
                    {
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to delete selected guest record
                        string sql;
                        sql = "Delete from COURSE where COURSE_ID = @CourseId";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@CourseId", txtCourseID.Text);

                        //execute delete. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();

                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record deleted successfully");
                            ClearForm();
                            cmbBoxSelectCourse.ResetText();
                            populateCourseCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error deleting record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {

                MessageBox.Show("You need to select a guest to delete it");
            }
        }

        private void frmCreateCourse_Load(object sender, EventArgs e)
        {
            populateCourseCombo();
            txtCourseID.ReadOnly = true;
        }
    }
}
