﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyPlaceSMS_Sawyer
{
    class Student
    {
        private int studentId;
        private string firstName;
        private string lastName;
        private int program;


        public Student()
        {

        }
        public Student(int studentId,string fName, string lName, int program)
        {
            StudentId = studentId;
            FirstName = fName;
            LastName = lName;
            Program = program;
        } 
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int Program { get => program; set => program = value; }
        public int StudentId { get => studentId; set => studentId = value; }

        //override toString to display firstName attibute value

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }


}
