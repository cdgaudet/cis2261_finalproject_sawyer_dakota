﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmCourseList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCourseList));
            this.dataGridViewCourseList = new System.Windows.Forms.DataGridView();
            this.courseId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.courseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.department = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourseList)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewCourseList
            // 
            this.dataGridViewCourseList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCourseList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.courseId,
            this.courseName,
            this.teacherLastName,
            this.teacherFirstName,
            this.department});
            this.dataGridViewCourseList.Location = new System.Drawing.Point(1, 1);
            this.dataGridViewCourseList.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewCourseList.Name = "dataGridViewCourseList";
            this.dataGridViewCourseList.RowHeadersWidth = 51;
            this.dataGridViewCourseList.RowTemplate.Height = 24;
            this.dataGridViewCourseList.Size = new System.Drawing.Size(799, 363);
            this.dataGridViewCourseList.TabIndex = 1;
            // 
            // courseId
            // 
            this.courseId.HeaderText = "Course ID";
            this.courseId.MinimumWidth = 6;
            this.courseId.Name = "courseId";
            this.courseId.ReadOnly = true;
            this.courseId.Width = 80;
            // 
            // courseName
            // 
            this.courseName.HeaderText = "Course Name";
            this.courseName.MinimumWidth = 6;
            this.courseName.Name = "courseName";
            this.courseName.ReadOnly = true;
            this.courseName.Width = 125;
            // 
            // teacherLastName
            // 
            this.teacherLastName.HeaderText = "Teacher Last Name";
            this.teacherLastName.MinimumWidth = 6;
            this.teacherLastName.Name = "teacherLastName";
            this.teacherLastName.ReadOnly = true;
            this.teacherLastName.Width = 175;
            // 
            // teacherFirstName
            // 
            this.teacherFirstName.HeaderText = "Teacher First Name";
            this.teacherFirstName.MinimumWidth = 6;
            this.teacherFirstName.Name = "teacherFirstName";
            this.teacherFirstName.ReadOnly = true;
            this.teacherFirstName.Width = 175;
            // 
            // department
            // 
            this.department.HeaderText = "Department";
            this.department.MinimumWidth = 6;
            this.department.Name = "department";
            this.department.Width = 190;
            // 
            // frmCourseList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 366);
            this.Controls.Add(this.dataGridViewCourseList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmCourseList";
            this.Text = "Happy Place College - Course List";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourseList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewCourseList;
        private System.Windows.Forms.DataGridViewTextBoxColumn courseId;
        private System.Windows.Forms.DataGridViewTextBoxColumn courseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn department;
    }
}