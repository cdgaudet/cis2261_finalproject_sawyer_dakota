﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != "admin" || txtPassword.Text != "123")
            {
                MessageBox.Show("Invalid Login Credentials.");
            } else
            {
                MessageBox.Show("Welcome, " + txtUsername.Text);
                (this.Owner as frmMain).loginToolStripMenuItem.Visible = false;
                (this.Owner as frmMain).logoutToolStripMenuItem.Visible = true;
                frmMain.loggedIn = true;
                this.Close();
            }
        }

        private void lblUsername_Click(object sender, EventArgs e)
        {

        }
    }
}
