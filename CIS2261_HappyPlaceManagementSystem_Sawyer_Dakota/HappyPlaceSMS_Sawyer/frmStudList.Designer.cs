﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmStuList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStuList));
            this.lbl_ProgName = new System.Windows.Forms.Label();
            this.txtProgName = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lbl_LName = new System.Windows.Forms.Label();
            this.txtLName = new System.Windows.Forms.TextBox();
            this.lbl_FName = new System.Windows.Forms.Label();
            this.txtFName = new System.Windows.Forms.TextBox();
            this.dataGridViewStudentList = new System.Windows.Forms.DataGridView();
            this.studentLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentProgram = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_ProgName
            // 
            this.lbl_ProgName.AutoSize = true;
            this.lbl_ProgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ProgName.Location = new System.Drawing.Point(299, 7);
            this.lbl_ProgName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_ProgName.Name = "lbl_ProgName";
            this.lbl_ProgName.Size = new System.Drawing.Size(115, 20);
            this.lbl_ProgName.TabIndex = 42;
            this.lbl_ProgName.Text = "Program Name";
            // 
            // txtProgName
            // 
            this.txtProgName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProgName.Location = new System.Drawing.Point(303, 34);
            this.txtProgName.Margin = new System.Windows.Forms.Padding(2);
            this.txtProgName.Name = "txtProgName";
            this.txtProgName.Size = new System.Drawing.Size(127, 26);
            this.txtProgName.TabIndex = 41;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(456, 28);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(107, 26);
            this.btnSearch.TabIndex = 40;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lbl_LName
            // 
            this.lbl_LName.AutoSize = true;
            this.lbl_LName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LName.Location = new System.Drawing.Point(10, 7);
            this.lbl_LName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_LName.Name = "lbl_LName";
            this.lbl_LName.Size = new System.Drawing.Size(90, 20);
            this.lbl_LName.TabIndex = 39;
            this.lbl_LName.Text = "Last Name:";
            // 
            // txtLName
            // 
            this.txtLName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLName.Location = new System.Drawing.Point(14, 34);
            this.txtLName.Margin = new System.Windows.Forms.Padding(2);
            this.txtLName.Name = "txtLName";
            this.txtLName.Size = new System.Drawing.Size(127, 26);
            this.txtLName.TabIndex = 38;
            // 
            // lbl_FName
            // 
            this.lbl_FName.AutoSize = true;
            this.lbl_FName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_FName.Location = new System.Drawing.Point(157, 7);
            this.lbl_FName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_FName.Name = "lbl_FName";
            this.lbl_FName.Size = new System.Drawing.Size(90, 20);
            this.lbl_FName.TabIndex = 37;
            this.lbl_FName.Text = "First Name:";
            // 
            // txtFName
            // 
            this.txtFName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFName.Location = new System.Drawing.Point(160, 34);
            this.txtFName.Margin = new System.Windows.Forms.Padding(2);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(127, 26);
            this.txtFName.TabIndex = 36;
            // 
            // dataGridViewStudentList
            // 
            this.dataGridViewStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.studentLastName,
            this.studentFirstName,
            this.studentEmail,
            this.studentAddress,
            this.studentPhone,
            this.studentProgram});
            this.dataGridViewStudentList.Location = new System.Drawing.Point(2, 63);
            this.dataGridViewStudentList.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewStudentList.Name = "dataGridViewStudentList";
            this.dataGridViewStudentList.RowHeadersWidth = 51;
            this.dataGridViewStudentList.RowTemplate.Height = 24;
            this.dataGridViewStudentList.Size = new System.Drawing.Size(804, 303);
            this.dataGridViewStudentList.TabIndex = 43;
            // 
            // studentLastName
            // 
            this.studentLastName.HeaderText = "Last Name";
            this.studentLastName.MinimumWidth = 6;
            this.studentLastName.Name = "studentLastName";
            this.studentLastName.ReadOnly = true;
            this.studentLastName.Width = 125;
            // 
            // studentFirstName
            // 
            this.studentFirstName.HeaderText = "First Name";
            this.studentFirstName.MinimumWidth = 6;
            this.studentFirstName.Name = "studentFirstName";
            this.studentFirstName.ReadOnly = true;
            this.studentFirstName.Width = 125;
            // 
            // studentEmail
            // 
            this.studentEmail.HeaderText = "Email";
            this.studentEmail.MinimumWidth = 6;
            this.studentEmail.Name = "studentEmail";
            this.studentEmail.Width = 125;
            // 
            // studentAddress
            // 
            this.studentAddress.HeaderText = "Address";
            this.studentAddress.MinimumWidth = 6;
            this.studentAddress.Name = "studentAddress";
            this.studentAddress.Width = 125;
            // 
            // studentPhone
            // 
            this.studentPhone.HeaderText = "Phone";
            this.studentPhone.MinimumWidth = 6;
            this.studentPhone.Name = "studentPhone";
            this.studentPhone.Width = 125;
            // 
            // studentProgram
            // 
            this.studentProgram.HeaderText = "Program Name";
            this.studentProgram.MinimumWidth = 6;
            this.studentProgram.Name = "studentProgram";
            this.studentProgram.ReadOnly = true;
            this.studentProgram.Width = 125;
            // 
            // frmStuList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 366);
            this.Controls.Add(this.dataGridViewStudentList);
            this.Controls.Add(this.lbl_ProgName);
            this.Controls.Add(this.txtProgName);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lbl_LName);
            this.Controls.Add(this.txtLName);
            this.Controls.Add(this.lbl_FName);
            this.Controls.Add(this.txtFName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStuList";
            this.Text = "Happy Place College - Student Listing";
            this.Load += new System.EventHandler(this.frmStuList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_ProgName;
        private System.Windows.Forms.TextBox txtProgName;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lbl_LName;
        private System.Windows.Forms.TextBox txtLName;
        private System.Windows.Forms.Label lbl_FName;
        private System.Windows.Forms.TextBox txtFName;
        public System.Windows.Forms.DataGridView dataGridViewStudentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentProgram;
    }
}