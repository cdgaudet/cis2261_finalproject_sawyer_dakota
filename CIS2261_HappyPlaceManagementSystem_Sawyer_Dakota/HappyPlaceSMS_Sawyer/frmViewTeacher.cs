﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmViewTeacher : Form
    {
        public frmViewTeacher()
        {
            InitializeComponent();
            PopulateTeacherCombo();
        }
        public void PopulateTeacherCombo()
        {

            //build connection to CISDB
            //create connection string for Cottages database
            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            //  ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT TEACHER.TEACHER_ID, TEACHER.TEACHER_LNAME, TEACHER.TEACHER_FNAME, TEACHER.ASSIGNED_COURSES FROM TEACHER ORDER BY TEACHER.TEACHER_LNAME, TEACHER.TEACHER_FNAME ASC;"; //note the two semicolons


                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a guest object populate the firstName and personId attibutes
                    //  Guest gue = new Guest(dbReader["FirstName"].ToString(), dbReader["LastName"].ToString(), dbReader["Street"].ToString(), dbReader["City"].ToString(), dbReader["State"].ToString(), dbReader["Zip"].ToString(), dbReader["Phone"].ToString(), dbReader["email"].ToString(), dbReader["LastVisitDate"].ToString(), dbReader["Room"].ToString(), (int)dbReader["GuestID"]);

                    //load the Guest object per into the combobox
                    //when displayed the combo box will call toString by default on the Guest object.
                    //the toString  displays the FirstName and LastName of the person.
                    //   cmbSelectGuest.Items.Add(gue);
                    dataGridViewStudentList.Rows.Add(dbReader["TEACHER_ID"].ToString(), dbReader["TEACHER_LNAME"].ToString(), dbReader["TEACHER_FNAME"].ToString(), dbReader["ASSIGNED_COURSES"].ToString());
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
