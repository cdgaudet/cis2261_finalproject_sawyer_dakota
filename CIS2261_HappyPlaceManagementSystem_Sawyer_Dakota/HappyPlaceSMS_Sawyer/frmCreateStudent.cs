﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmCreateStudent : Form
    {
        // frmStudentList fsl;
        public frmCreateStudent()
        {
            InitializeComponent();
            //  fsl= fs;


        }
        //build connection to CISDB
        //create connection string for Cottages database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
        //Create OldDbConnection
        OleDbConnection dbConn;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // frmStudentList fsl = new frmStudentList();

            if (cmbBoxSelectStudent.SelectedIndex >= 0)
            {
                ClearForm();
                cmbBoxSelectStudent.ResetText();
                populateStudentCombo();
            }
            else
            {
                if (validateForm() == true)
                {
                    addStudent();
                }

            }
        }



        public void populateStudentCombo()
        {
            ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT STUDENT_ID,STUDENT_FNAME,STUDENT_LNAME,EMAIL,ADDRESS,PHONE,PROGRAM_ID from  STUDENT;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a guest object populate the firstName and personId attibutes
                    Student stu = new Student((int)dbReader["STUDENT_ID"], dbReader["STUDENT_FNAME"].ToString(), dbReader["STUDENT_LNAME"].ToString(), (int)dbReader["PROGRAM_ID"]);

                    //load the Guest object per into the combobox
                    //when displayed the combo box will call toString by default on the Guest object.
                    //the toString  displays the FirstName and LastName of the person.
                    cmbBoxSelectStudent.Items.Add(stu);

                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void ClearForm()
        {
            cmbBoxSelectStudent.Items.Clear();
            txtFirstName.Clear();
            txtLastName.Clear();
            txtStudentID.Clear();
            txtEmail.Clear();
            txtAddress.Clear();
            txtPhone.Clear();
            txtProgram.Clear();
        }

        private void addStudent()
        {
            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Guests table
                string sql;
                sql = "Insert into STUDENT(STUDENT_FNAME, STUDENT_LNAME, EMAIL, ADDRESS, PHONE, PROGRAM_ID) Values (@StudentFirstName, @StudentLastName, @Email, @Address, @Phone, @ProgramID);"; //note the two semicolons

                //create database command
                OleDbCommand dbCmd = new OleDbCommand();

                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;

                //bind parameters
                dbCmd.Parameters.AddWithValue("@StudentFirstName", txtFirstName.Text);
                dbCmd.Parameters.AddWithValue("@StudentLastName", txtLastName.Text);
                dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                dbCmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
                dbCmd.Parameters.AddWithValue("@ProgramID", txtProgram.Text);


                //MessageBox.Show(txtPhone.Text);
                //execute insert. Check to see how many rows were affected
                int rowCount = dbCmd.ExecuteNonQuery();

                //close database connection
                dbConn.Close();
                if (rowCount == 1)
                {
                    MessageBox.Show("Record inserted successfully");
                    //this is where I want to update frmMain
                    populateStudentCombo();

                }
                else
                {
                    MessageBox.Show("Error inserting record. Please try again.");
                    ClearForm();
                    cmbBoxSelectStudent.ResetText();
                    populateStudentCombo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private bool validateForm()
        {
            string errMsg = "";
            if (txtFirstName.Text == "")
            {
                errMsg += "Missing first name. \n";
            }
            if (txtLastName.Text == "")
            {
                errMsg += "Missing last name. \n";
            }
            if (txtEmail.Text == "")
            {
                errMsg += "Missing email. \n";
            }
            if (txtAddress.Text == "")
            {
                errMsg += "Missing address. \n";
            }
            if (txtPhone.Text == "")
            {
                errMsg += "Missing phone number. \n";
            }
            if (txtProgram.Text == "")
            {
                errMsg += "Missing program. \n";
            }


            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void cmbBoxSelectStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateStudentRead();
        }
        private void PopulateStudentRead()
        {
            //the combobox is populated with Guest objects. Cast the selected value as a Guest and then you can 
            //access the public property GuestId;
            int studentSelection = ((Student)cmbBoxSelectStudent.SelectedItem).StudentId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Guests table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the GuestId to search for
                sql = "SELECT(Select count(STUDENT_ID) from STUDENT where STUDENT_ID= " + studentSelection + ") " +
                               "as rowCount, * from STUDENT where STUDENT_ID = " + studentSelection + ";"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                // MessageBox.Show(sql);
                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes

                    txtStudentID.Text = dbReader["STUDENT_ID"].ToString();
                    txtFirstName.Text = dbReader["STUDENT_FNAME"].ToString();
                    txtLastName.Text = dbReader["STUDENT_LNAME"].ToString();
                    txtEmail.Text = dbReader["EMAIL"].ToString();
                    txtAddress.Text = dbReader["ADDRESS"].ToString();
                    txtPhone.Text = dbReader["PHONE"].ToString();
                    txtProgram.Text = dbReader["PROGRAM_ID"].ToString();
                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void frmCreateStudent_Load(object sender, EventArgs e)
        {
            populateStudentCombo();
            txtStudentID.ReadOnly = true;
        }

        private void btnUpdateStudent_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectStudent.SelectedIndex > -1)
            {
                //Check to see if they want to update the current record
                DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //update the record
                    try
                    {
                        //MessageBox.Show("Update");
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to update selected guest record
                        string sql;
                        sql = "Update STUDENT set STUDENT_LNAME = @LastName , STUDENT_FNAME =  @FirstName , EMAIL = @Email, ADDRESS = @Address, PHONE = @Phone, PROGRAM_ID = @Program where STUDENT_ID = @StudentID";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
                        dbCmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                        dbCmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                        dbCmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                        dbCmd.Parameters.AddWithValue("@Phone", txtPhone.Text);
                        dbCmd.Parameters.AddWithValue("@Program", txtProgram.Text);
                        dbCmd.Parameters.AddWithValue("@StudentID", txtStudentID.Text);
                        //  MessageBox.Show(dateTimeLastVisitDate.Value.ToShortDateString());
                        //MessageBox.Show(sql);
                        //execute update. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();
                        // MessageBox.Show(rowCount.ToString());
                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record updated successfully");
                            ClearForm();
                            cmbBoxSelectStudent.ResetText();
                            populateStudentCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error updating record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {

                    MessageBox.Show("Please select the students you want to update");
                }
            }
        }

        private void btnDeleteStudent_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectStudent.SelectedIndex > 0)
            {
                //Check to see if they want to delete the current record
                DialogResult result = MessageBox.Show("Do you want to delete this record?", "Delete Record",
                MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //update the record
                    try
                    {
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to delete selected guest record
                        string sql;
                        sql = "Delete from STUDENT where STUDENT_ID = @StudentID";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@StudentID", txtStudentID.Text);

                        //execute delete. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();

                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record deleted successfully");
                            ClearForm();
                            cmbBoxSelectStudent.ResetText();
                            populateStudentCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error deleting record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else
            {

                MessageBox.Show("You need to select a student to delete");
            }
        }
    }
}








