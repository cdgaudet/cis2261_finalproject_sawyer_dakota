﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmViewBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewBill));
            this.dataGridViewStudentList = new System.Windows.Forms.DataGridView();
            this.billId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.billAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.assignedStudents = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.billDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewStudentList
            // 
            this.dataGridViewStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.billId,
            this.billAmount,
            this.assignedStudents,
            this.billDescription});
            this.dataGridViewStudentList.Location = new System.Drawing.Point(1, 1);
            this.dataGridViewStudentList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewStudentList.Name = "dataGridViewStudentList";
            this.dataGridViewStudentList.RowHeadersWidth = 51;
            this.dataGridViewStudentList.RowTemplate.Height = 24;
            this.dataGridViewStudentList.Size = new System.Drawing.Size(1059, 449);
            this.dataGridViewStudentList.TabIndex = 1;
            // 
            // billId
            // 
            this.billId.HeaderText = "Bill ID";
            this.billId.MinimumWidth = 6;
            this.billId.Name = "billId";
            this.billId.ReadOnly = true;
            this.billId.Width = 80;
            // 
            // billAmount
            // 
            this.billAmount.HeaderText = "Bill Amount";
            this.billAmount.MinimumWidth = 6;
            this.billAmount.Name = "billAmount";
            this.billAmount.Width = 125;
            // 
            // assignedStudents
            // 
            this.assignedStudents.HeaderText = "Assigned Student";
            this.assignedStudents.MinimumWidth = 6;
            this.assignedStudents.Name = "assignedStudents";
            this.assignedStudents.Width = 145;
            // 
            // billDescription
            // 
            this.billDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.billDescription.HeaderText = "Description";
            this.billDescription.MinimumWidth = 6;
            this.billDescription.Name = "billDescription";
            // 
            // frmViewBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 450);
            this.Controls.Add(this.dataGridViewStudentList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmViewBill";
            this.Text = "Happy Place College - View Bill";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dataGridViewStudentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn billId;
        private System.Windows.Forms.DataGridViewTextBoxColumn billAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn billDescription;
    }
}