﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmUpdateBill : Form
    {
        public frmUpdateBill()
        {
            InitializeComponent();
        }

        //build connection to CISDB
        //create connection string for Cottages database
        string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
        //Create OldDbConnection
        OleDbConnection dbConn;

        public void populateBillCombo()
        {
            ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT BILL_ID, BILL_AMOUNT, ASSIGNED_STUDENT, DESCRIPTION, AMOUNT_PAID, DATE_OF_PAYMENT from BILL;"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a teacher object populate the firstName and personId attibutes
                    Bill bill = new Bill((int)dbReader["BILL_ID"], Convert.ToDouble(dbReader["BILL_AMOUNT"]), dbReader["ASSIGNED_STUDENT"].ToString(), dbReader["DESCRIPTION"].ToString(), Convert.ToDouble(dbReader["AMOUNT_PAID"]), dbReader["DATE_OF_PAYMENT"].ToString());

                    //load the teacher object per into the combobox
                    //when displayed the combo box will call toString by default on the teacher object.
                    //the toString  displays the FirstName and LastName of the person.
                    cmbBoxSelectBill.Items.Add(bill);

                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ClearForm()
        {
            cmbBoxSelectBill.Items.Clear();
            txtAmountPaid.Clear();
            txtBillId.Clear();
            txtAssignedStudent.Clear();
            txtBillAmount.Clear();
            rtbDescription.Clear();
        }

        private bool validateForm()
        {
            string errMsg = "";
            if (txtAmountPaid.Text == "")
            {
                errMsg += "Missing Amount Paid. \n";
            }
            if (txtAssignedStudent.Text == "")
            {
                errMsg += "Missing Assigned Student. \n";
            }
            if (txtBillAmount.Text == "")
            {
                errMsg += "Missing Bill Amount. \n";
            }
            if (dtpDateOfPayment.Text == "")
            {
                errMsg += "Missing Date. \n";
            }

            if (errMsg.Length > 0)
            {
                MessageBox.Show(errMsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void PopulateBillRead()
        {
            //the combobox is populated with teacher objects. Cast the selected value as a teacher and then you can 
            //access the public property teacherId;
            int billSelection = ((Bill)cmbBoxSelectBill.SelectedItem).BillId;

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from teachers table
                string sql;

                //Add a subquery to get the record count
                //Using a variable to identify the teacherId to search for
                sql = "SELECT(Select count(BILL_ID) from BILL where BILL_ID = " + billSelection + ") " +
                               "as rowCount, * from BILL where BILL_ID = " + billSelection + ";"; //note the two semicolons

                OleDbCommand dbCmd = new OleDbCommand();
                // MessageBox.Show(sql);
                //set command SQL string
                dbCmd.CommandText = sql;

                //set the command connection
                dbCmd.Connection = dbConn;

                //get number of rows
                //ExecuteScalar just returns the value of the first column
                int numRows = (Int32)dbCmd.ExecuteScalar();

                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;

                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                dbReader.Read();
                if (dbReader.HasRows && numRows == 1)
                {
                    //get data from dbReader by column name and assing to text boxes

                    txtBillId.Text = dbReader["BILL_ID"].ToString();
                    txtAmountPaid.Text = string.Format("{0:#.00}", Convert.ToDecimal(dbReader["AMOUNT_PAID"].ToString()));
                    txtAssignedStudent.Text = dbReader["ASSIGNED_STUDENT"].ToString();
                    txtBillAmount.Text = string.Format("{0:#.00}", Convert.ToDecimal(dbReader["BILL_AMOUNT"].ToString()));
                    dtpDateOfPayment.Text = dbReader["DATE_OF_PAYMENT"].ToString();
                    rtbDescription.Text = dbReader["DESCRIPTION"].ToString();

                }
                //Close open connections
                dbReader.Close();
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void frmUpdateBill_Load(object sender, EventArgs e)
        {
            populateBillCombo();
            txtBillId.ReadOnly = true;
        }

        private void cmbBoxSelectBill_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateBillRead();
        }

        private void btnUpdateBill_Click(object sender, EventArgs e)
        {
            if (cmbBoxSelectBill.SelectedIndex > -1)
            {
                //Check to see if they want to update the current record
                DialogResult result = MessageBox.Show("Do you want to update this record?", "Update Record",
                    MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    int currentBillID = Convert.ToInt32(txtBillId.Text);
                    double newBillAmount = Convert.ToDouble(txtBillAmount.Text) - Convert.ToDouble(txtAmountPaid.Text);
                    //update the record
                    try
                    {
                        dbConn = new OleDbConnection(sConnection);
                        //open connection to database
                        dbConn.Open();
                        //create query to update selected teacher record
                        string sql;
                        sql = "UPDATE BILL SET BILL_AMOUNT = @BillAmount, ASSIGNED_STUDENT = @AssignedStudent, DESCRIPTION = @Description, AMOUNT_PAID = @AmountPaid, DATE_OF_PAYMENT = @DateOfPayment WHERE BILL_ID = " + currentBillID + ";";

                        //create database command
                        OleDbCommand dbCmd = new OleDbCommand();

                        //set command SQL string
                        dbCmd.CommandText = sql;
                        //set the command connection
                        dbCmd.Connection = dbConn;

                        //bind parameters
                        dbCmd.Parameters.AddWithValue("@BillAmount", newBillAmount);
                        dbCmd.Parameters.AddWithValue("@AssignedStudent", txtAssignedStudent.Text);
                        dbCmd.Parameters.AddWithValue("@Description", rtbDescription.Text);
                        dbCmd.Parameters.AddWithValue("@AmountPaid", txtAmountPaid.Text);
                        dbCmd.Parameters.AddWithValue("@DateOfPayment", dtpDateOfPayment.Value);

                        //execute update. Check to see how many rows were affected
                        int rowCount = dbCmd.ExecuteNonQuery();

                        //close database connection
                        dbConn.Close();
                        // MessageBox.Show(rowCount.ToString());
                        if (rowCount == 1)
                        {
                            MessageBox.Show("Record updated successfully");
                            ClearForm();
                            cmbBoxSelectBill.ResetText();
                            populateBillCombo();
                        }
                        else
                        {
                            MessageBox.Show("Error updating record. Please try again.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {

                    MessageBox.Show("Please select the teacher you want to update");
                }
            }
        }

    }
}
