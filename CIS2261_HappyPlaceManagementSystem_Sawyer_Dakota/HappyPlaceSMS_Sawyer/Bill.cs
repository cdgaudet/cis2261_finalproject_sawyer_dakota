﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyPlaceSMS_Sawyer
{
    class Bill
    {
        private int billId;
        private double billAmount;
        private string billAssignedStudent;
        private string billDescription;
        private double billAmountPaid;
        private string billDateOfPayment;
        public Bill()
        {
        }
        public Bill(int billId, double billAmount, string billAssignedStudent, string billDescription, double billAmountPaid, string billDateOfPayment)
        {
            BillId = billId;
            BillAmount = billAmount;
            BillAssignedStudent = billAssignedStudent;
            BillDescription = billDescription;
            BillAmountPaid = billAmountPaid;
            BillDateOfPayment = billDateOfPayment;

        }

        public int BillId { get => billId; set => billId = value; }
        public double BillAmount { get => billAmount; set => billAmount = value; }
        public string BillAssignedStudent { get => billAssignedStudent; set => billAssignedStudent = value; }
        public string BillDescription { get => billDescription; set => billDescription = value; }
        public double BillAmountPaid { get => billAmountPaid; set => billAmountPaid = value; }
        public string BillDateOfPayment { get => billDateOfPayment; set => billDateOfPayment = value; }

        public override string ToString()
        {
            return BillId + " - " + BillAssignedStudent;
        }
    }
}
