﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip_Main = new System.Windows.Forms.MenuStrip();
            this.iRSMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teachersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Patients = new System.Windows.Forms.ToolStripMenuItem();
            this.createStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coursesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createCourseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCourseCatalogueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.billingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facultyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facultyReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip_Main
            // 
            this.menuStrip_Main.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iRSMToolStripMenuItem,
            this.teachersToolStripMenuItem,
            this.Patients,
            this.coursesToolStripMenuItem,
            this.billingToolStripMenuItem,
            this.facultyToolStripMenuItem,
            this.loginToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip_Main.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_Main.Name = "menuStrip_Main";
            this.menuStrip_Main.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip_Main.Size = new System.Drawing.Size(600, 36);
            this.menuStrip_Main.TabIndex = 43;
            this.menuStrip_Main.Text = "menuStrip1";
            // 
            // iRSMToolStripMenuItem
            // 
            this.iRSMToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.iRSMToolStripMenuItem.Name = "iRSMToolStripMenuItem";
            this.iRSMToolStripMenuItem.Size = new System.Drawing.Size(64, 32);
            this.iRSMToolStripMenuItem.Text = "SMS";
            // 
            // teachersToolStripMenuItem
            // 
            this.teachersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTeacherToolStripMenuItem,
            this.viewTeacherToolStripMenuItem});
            this.teachersToolStripMenuItem.Name = "teachersToolStripMenuItem";
            this.teachersToolStripMenuItem.Size = new System.Drawing.Size(64, 32);
            this.teachersToolStripMenuItem.Text = "Teachers";
            // 
            // createTeacherToolStripMenuItem
            // 
            this.createTeacherToolStripMenuItem.Name = "createTeacherToolStripMenuItem";
            this.createTeacherToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.createTeacherToolStripMenuItem.Text = "Create/Edit Teacher";
            this.createTeacherToolStripMenuItem.Click += new System.EventHandler(this.createTeacherToolStripMenuItem_Click);
            // 
            // viewTeacherToolStripMenuItem
            // 
            this.viewTeacherToolStripMenuItem.Name = "viewTeacherToolStripMenuItem";
            this.viewTeacherToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewTeacherToolStripMenuItem.Text = "View Teacher";
            this.viewTeacherToolStripMenuItem.Click += new System.EventHandler(this.viewTeacherToolStripMenuItem_Click);
            // 
            // Patients
            // 
            this.Patients.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createStudentToolStripMenuItem,
            this.viewStudentToolStripMenuItem,
            this.listStudentsToolStripMenuItem});
            this.Patients.Name = "Patients";
            this.Patients.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Patients.Size = new System.Drawing.Size(65, 32);
            this.Patients.Text = "Students";
            // 
            // createStudentToolStripMenuItem
            // 
            this.createStudentToolStripMenuItem.Name = "createStudentToolStripMenuItem";
            this.createStudentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.createStudentToolStripMenuItem.Text = "Create/Edit Student";
            this.createStudentToolStripMenuItem.Click += new System.EventHandler(this.createStudentToolStripMenuItem_Click);
            // 
            // viewStudentToolStripMenuItem
            // 
            this.viewStudentToolStripMenuItem.Name = "viewStudentToolStripMenuItem";
            this.viewStudentToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewStudentToolStripMenuItem.Text = "View Student";
            this.viewStudentToolStripMenuItem.Click += new System.EventHandler(this.viewStudentToolStripMenuItem_Click);
            // 
            // listStudentsToolStripMenuItem
            // 
            this.listStudentsToolStripMenuItem.Name = "listStudentsToolStripMenuItem";
            this.listStudentsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listStudentsToolStripMenuItem.Text = "List Students";
            this.listStudentsToolStripMenuItem.Click += new System.EventHandler(this.listStudentsToolStripMenuItem_Click);
            // 
            // coursesToolStripMenuItem
            // 
            this.coursesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCourseToolStripMenuItem,
            this.viewCourseCatalogueToolStripMenuItem1});
            this.coursesToolStripMenuItem.Name = "coursesToolStripMenuItem";
            this.coursesToolStripMenuItem.Size = new System.Drawing.Size(61, 32);
            this.coursesToolStripMenuItem.Text = "Courses";
            // 
            // createCourseToolStripMenuItem
            // 
            this.createCourseToolStripMenuItem.Name = "createCourseToolStripMenuItem";
            this.createCourseToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.createCourseToolStripMenuItem.Text = "Create/Edit Course";
            this.createCourseToolStripMenuItem.Click += new System.EventHandler(this.createCourseToolStripMenuItem_Click);
            // 
            // viewCourseCatalogueToolStripMenuItem1
            // 
            this.viewCourseCatalogueToolStripMenuItem1.Name = "viewCourseCatalogueToolStripMenuItem1";
            this.viewCourseCatalogueToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.viewCourseCatalogueToolStripMenuItem1.Text = "View Course Catalogue";
            this.viewCourseCatalogueToolStripMenuItem1.Click += new System.EventHandler(this.viewCourseCatalogueToolStripMenuItem1_Click);
            // 
            // billingToolStripMenuItem
            // 
            this.billingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createBillToolStripMenuItem,
            this.viewBillToolStripMenuItem,
            this.updateBillToolStripMenuItem});
            this.billingToolStripMenuItem.Name = "billingToolStripMenuItem";
            this.billingToolStripMenuItem.Size = new System.Drawing.Size(52, 32);
            this.billingToolStripMenuItem.Text = "Billing";
            // 
            // createBillToolStripMenuItem
            // 
            this.createBillToolStripMenuItem.Name = "createBillToolStripMenuItem";
            this.createBillToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.createBillToolStripMenuItem.Text = "Create Bill";
            this.createBillToolStripMenuItem.Click += new System.EventHandler(this.createBillToolStripMenuItem_Click);
            // 
            // viewBillToolStripMenuItem
            // 
            this.viewBillToolStripMenuItem.Name = "viewBillToolStripMenuItem";
            this.viewBillToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewBillToolStripMenuItem.Text = "View Bill";
            this.viewBillToolStripMenuItem.Click += new System.EventHandler(this.viewBillToolStripMenuItem_Click);
            // 
            // updateBillToolStripMenuItem
            // 
            this.updateBillToolStripMenuItem.Name = "updateBillToolStripMenuItem";
            this.updateBillToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.updateBillToolStripMenuItem.Text = "Update Bill";
            this.updateBillToolStripMenuItem.Click += new System.EventHandler(this.updateBillToolStripMenuItem_Click);
            // 
            // facultyToolStripMenuItem
            // 
            this.facultyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.facultyReportToolStripMenuItem});
            this.facultyToolStripMenuItem.Name = "facultyToolStripMenuItem";
            this.facultyToolStripMenuItem.Size = new System.Drawing.Size(57, 32);
            this.facultyToolStripMenuItem.Text = "Faculty";
            // 
            // facultyReportToolStripMenuItem
            // 
            this.facultyReportToolStripMenuItem.Name = "facultyReportToolStripMenuItem";
            this.facultyReportToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.facultyReportToolStripMenuItem.Text = "Faculty Report";
            this.facultyReportToolStripMenuItem.Click += new System.EventHandler(this.facultyReportToolStripMenuItem_Click);
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(49, 32);
            this.loginToolStripMenuItem.Text = "Login";
            this.loginToolStripMenuItem.Click += new System.EventHandler(this.loginToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(57, 32);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(600, 331);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Happy Place College - Student Management System";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip_Main.ResumeLayout(false);
            this.menuStrip_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip_Main;
        private System.Windows.Forms.ToolStripMenuItem iRSMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teachersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Patients;
        private System.Windows.Forms.ToolStripMenuItem createStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coursesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createCourseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewCourseCatalogueToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facultyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facultyReportToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewTeacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewBillToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem updateBillToolStripMenuItem;
    }
}

