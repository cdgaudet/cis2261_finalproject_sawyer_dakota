﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HappyPlaceSMS_Sawyer
{
    public partial class frmViewStudent : Form
    {
        public frmViewStudent()
        {
            InitializeComponent();
            PopulateStudentCombo();
        }

        public void PopulateStudentCombo()
        {

            //build connection to CISDB
            //create connection string for Cottages database
            string sConnection = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=HPMS.accdb";
            //Create OldDbConnection
            OleDbConnection dbConn;
            //  ClearForm();

            try
            {
                dbConn = new OleDbConnection(sConnection);
                //open connection to database
                dbConn.Open();
                //create query to select all rows from Person table
                string sql;
                sql = "SELECT STUDENT.STUDENT_LNAME, STUDENT.STUDENT_FNAME, STUDENT.EMAIL, STUDENT.ADDRESS, STUDENT.PHONE, PROGRAM.PROGRAM_NAME FROM PROGRAM INNER JOIN STUDENT ON PROGRAM.PROGRAM_ID = STUDENT.PROGRAM_ID ORDER BY STUDENT.STUDENT_LNAME, STUDENT.STUDENT_FNAME ASC;"; //note the two semicolons


                OleDbCommand dbCmd = new OleDbCommand();
                //set command SQL string
                dbCmd.CommandText = sql;
                //set the command connection
                dbCmd.Connection = dbConn;
                //create OleDbDataReader dbReader
                OleDbDataReader dbReader;
                //Read data into dbReader
                dbReader = dbCmd.ExecuteReader();

                //Read first record
                while (dbReader.Read())
                {

                    //Create a guest object populate the firstName and personId attibutes
                    //  Guest gue = new Guest(dbReader["FirstName"].ToString(), dbReader["LastName"].ToString(), dbReader["Street"].ToString(), dbReader["City"].ToString(), dbReader["State"].ToString(), dbReader["Zip"].ToString(), dbReader["Phone"].ToString(), dbReader["email"].ToString(), dbReader["LastVisitDate"].ToString(), dbReader["Room"].ToString(), (int)dbReader["GuestID"]);

                    //load the Guest object per into the combobox
                    //when displayed the combo box will call toString by default on the Guest object.
                    //the toString  displays the FirstName and LastName of the person.
                    //   cmbSelectGuest.Items.Add(gue);
                    studentlistdataGridViewStudentList.Rows.Add(dbReader["STUDENT_LNAME"].ToString(), dbReader["STUDENT_FNAME"].ToString(), dbReader["EMAIL"].ToString(), dbReader["ADDRESS"].ToString(), dbReader["PHONE"].ToString(), dbReader["PROGRAM_NAME"].ToString());
                }
                //close Reader
                dbReader.Close();
                //close database connection
                dbConn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

    }
}
