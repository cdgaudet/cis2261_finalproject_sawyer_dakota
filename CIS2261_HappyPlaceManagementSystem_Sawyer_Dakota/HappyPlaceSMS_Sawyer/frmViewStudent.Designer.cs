﻿
namespace HappyPlaceSMS_Sawyer
{
    partial class frmViewStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmViewStudent));
            this.studentlistdataGridViewStudentList = new System.Windows.Forms.DataGridView();
            this.studentLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentProgram = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.studentlistdataGridViewStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // studentlistdataGridViewStudentList
            // 
            this.studentlistdataGridViewStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentlistdataGridViewStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.studentLastName,
            this.studentFirstName,
            this.studentEmail,
            this.studentAddress,
            this.studentPhone,
            this.studentProgram});
            this.studentlistdataGridViewStudentList.Location = new System.Drawing.Point(0, 0);
            this.studentlistdataGridViewStudentList.Margin = new System.Windows.Forms.Padding(2);
            this.studentlistdataGridViewStudentList.Name = "studentlistdataGridViewStudentList";
            this.studentlistdataGridViewStudentList.RowHeadersWidth = 51;
            this.studentlistdataGridViewStudentList.RowTemplate.Height = 24;
            this.studentlistdataGridViewStudentList.Size = new System.Drawing.Size(815, 368);
            this.studentlistdataGridViewStudentList.TabIndex = 0;
            // 
            // studentLastName
            // 
            this.studentLastName.HeaderText = "Last Name";
            this.studentLastName.MinimumWidth = 6;
            this.studentLastName.Name = "studentLastName";
            this.studentLastName.ReadOnly = true;
            this.studentLastName.Width = 125;
            // 
            // studentFirstName
            // 
            this.studentFirstName.HeaderText = "First Name";
            this.studentFirstName.MinimumWidth = 6;
            this.studentFirstName.Name = "studentFirstName";
            this.studentFirstName.ReadOnly = true;
            this.studentFirstName.Width = 125;
            // 
            // studentEmail
            // 
            this.studentEmail.HeaderText = "Email";
            this.studentEmail.MinimumWidth = 6;
            this.studentEmail.Name = "studentEmail";
            this.studentEmail.Width = 125;
            // 
            // studentAddress
            // 
            this.studentAddress.HeaderText = "Address";
            this.studentAddress.MinimumWidth = 6;
            this.studentAddress.Name = "studentAddress";
            this.studentAddress.Width = 125;
            // 
            // studentPhone
            // 
            this.studentPhone.HeaderText = "Phone";
            this.studentPhone.MinimumWidth = 6;
            this.studentPhone.Name = "studentPhone";
            this.studentPhone.Width = 125;
            // 
            // studentProgram
            // 
            this.studentProgram.HeaderText = "Program Name";
            this.studentProgram.MinimumWidth = 6;
            this.studentProgram.Name = "studentProgram";
            this.studentProgram.ReadOnly = true;
            this.studentProgram.Width = 140;
            // 
            // frmViewStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(815, 366);
            this.Controls.Add(this.studentlistdataGridViewStudentList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmViewStudent";
            this.Text = "Happy Place College - View Student";
            ((System.ComponentModel.ISupportInitialize)(this.studentlistdataGridViewStudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.DataGridView studentlistdataGridViewStudentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentProgram;
    }
}